const std = @import("std");

const Initial = packed struct {
    additional: u5 = 0,
    major: u3 = 0,
};

const Major = enum(u3) {
    unsigned_int = 0,
    negative_int = 1,
    byte_string = 2,
    text_string = 3,
    array = 4,
    map = 5,
    tag = 6,
    float_or_simple = 7,
};

const Additional = enum(u5) {
    byte = 24,
    short = 25,
    long = 26,
    long_long = 27,
};

fn encodeInitial(major: Major, value: i32, writer: anytype) !void {
    const p: u32 = if (value < 0) @intCast(u32, -1 - value) else @intCast(u32, value);
    var initial: Initial = .{ .major = @enumToInt(major) };
    if (p < 24) {
        initial.additional = @truncate(u5, p);
        try writer.writeByte(@bitCast(u8, initial));
    } else if (p <= 2 << 8 - 1) {
        initial.additional = @enumToInt(Additional.byte);
        try writer.writeByte(@bitCast(u8, initial));
        try writer.writeByte(@truncate(u8, p));
    } else if (p <= 2 << 16 - 1) {
        initial.additional = @enumToInt(Additional.short);
        try writer.writeByte(@bitCast(u8, initial));
        try writer.writeByte(@truncate(u8, p >> 8));
        try writer.writeByte(@truncate(u8, p));
    } else if (p <= 2 << 32 - 1) {
        initial.additional = @enumToInt(Additional.long);
        try writer.writeByte(@bitCast(u8, initial));
        try writer.writeByte(@truncate(u8, p >> 24));
        try writer.writeByte(@truncate(u8, p >> 16));
        try writer.writeByte(@truncate(u8, p >> 8));
        try writer.writeByte(@truncate(u8, p));
    }
}

pub fn encodeInt(n: i32, writer: anytype) !void {
    try encodeInitial(if (n < 0) .negative_int else .unsigned_int, n, writer);
}

pub fn encodeByteString(s: []const u8, writer: anytype) !void {
    try encodeInitial(.byte_string, @intCast(i32, s.len), writer);
    try writer.writeAll(s);
}

pub fn encodeTextString(s: []const u8, writer: anytype) !void {
    try encodeInitial(.text_string, @intCast(i32, s.len), writer);
    try writer.writeAll(s);
}

pub fn encodeArray(len: u31, writer: anytype) !void {
    try encodeInitial(.array, @intCast(i32, len), writer);
}

pub fn encodeMap(items: u31, writer: anytype) !void {
    try encodeInitial(.map, @intCast(i32, items), writer);
}

pub fn encodeTag(tag: u31, writer: anytype) !void {
    try encodeInitial(.tag, @intCast(i32, tag), writer);
}

pub fn encodeDecimalFraction(mantissa: i31, exp: i31, writer: anytype) !void {
    try encodeTag(4, writer);
    try encodeArray(2, writer);
    try encodeInt(exp, writer);
    try encodeInt(mantissa, writer);
}

test "Encode integer" {
    var buffer: [1024]u8 = undefined;
    var fbs: std.io.FixedBufferStream([]u8) = .{ .buffer = &buffer, .pos = 0 };
    var writer = fbs.writer();
    try encodeInt(0x40808080, writer);
    std.debug.print("\n{s}\n", .{std.fmt.fmtSliceHexUpper(fbs.getWritten())});
    fbs.pos = 0;
    try encodeByteString("abc", writer);
    std.debug.print("{s}\n", .{std.fmt.fmtSliceHexUpper(fbs.getWritten())});
    fbs.pos = 0;
    try encodeDecimalFraction(27315, -2, writer);
    std.debug.print("{s}\n", .{std.fmt.fmtSliceHexUpper(fbs.getWritten())});
}
